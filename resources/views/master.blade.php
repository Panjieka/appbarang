<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="{{asset('/material-dashboard-dark-edition-v2.1.0/assets/img/apple-icon.png')}}">
  <link rel="icon" type="image/png" href="{{asset('/material-dashboard-dark-edition-v2.1.0/assets/img/favicon.png')}}">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="{{asset('/material-dashboard-dark-edition-v2.1.0/assets/css/material-dashboard.css?v=2.1.0')}}" rel="stylesheet" />
</head>

<body class="dark-edition">
  <div class="wrapper ">
    

      @include('part.sidebar')
      <!-- Navbar -->
      @include('part.nav')
      <!-- End Navbar -->

  <!-- Tabel -->      
  @yield('content')
        
  <!--   Core JS Files   -->
  <script src="{{asset('/material-dashboard-dark-edition-v2.1.0/assets/js/core/jquery.min.js')}}"></script>
  <script src="{{asset('/material-dashboard-dark-edition-v2.1.0/assets/js/core/popper.min.js')}}"></script>
  <script src="{{asset('/material-dashboard-dark-edition-v2.1.0/assets/js/core/bootstrap-material-design.min.js')}}"></script>
  <script src="https://unpkg.com/default-passive-events"></script>
  <script src="{{asset('/material-dashboard-dark-edition-v2.1.0/assets/js/plugins/perfect-scrollbar.jquery.min.js')}}"></script>
</body>

</html>