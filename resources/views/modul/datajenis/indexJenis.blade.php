@extends('master')

@section('content')
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header card-header-primary">
                <h4 class="card-title ">Tabel Jenis Barang</h4>
                <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#tambahData">
                  <i class="fa fa-plus"></i> Tambah
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    <!-- Button trigger modal -->

    <!-- Modal -->
    <div class="modal fade" id="tambahData" aria-labelledby="tambahDataLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="tambahDataLabel" style="color: black">Jenis Barang</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form action="/jenis-barang" method="post">
            @csrf
              <div class="modal-body">
                <div class="form-group">
                    <label for="nama" style="color: black">Jenis Barang</label>
                    <input type="text" name="nama" id="nama" class="form-control" style="color: black">
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
          </form>
        </div>
      </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
        <table class="table text-black" id="dtcate" width="100%" cellspacing="0">
            <thead>
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($category as $item)
                <tr>
                    <td>{{ !empty($i) ? ++$i : $i = 1 }}</td>
                    <td>{{ $item->nama }}</td>
                    <td>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editData{{$item->id}}">
                          <i class='fa fa-edit px-1'></i>
                        </button>
                        <form action="{{ url("/jenis-barang/$item->id") }}" method="POST" class="d-sm-inline">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger"><i class="fa fa-trash px-1" data-id="{{$item->id}}"></i></button>
                        </form>
                    </td>
                </tr>

                <div class="modal fade" id="editData{{$item->id}}" tabindex="-1" aria-labelledby="editDataLabel{{$item->id}}" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="editDataLabel{{$item->id}}" style="color: black">Edit Jenis Barang</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <form action="/jenis-barang/{{$item->id}}" method="post">
                        @csrf
                        @method('put')
                          <div class="modal-body">
                            <div class="form-group">
                                <label for="nama" style="color: black">Jenis Barang</label>
                                <input type="text" name="nama" id="nama" class="form-control" value="{{$item->nama}}" style="color: black">
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                          </div>
                      </form>
                    </div>
                  </div>
                </div>



                @endforeach
            </tbody>
        </table>

        </div>
    </div>
</div>
@endsection
@if(session('success'))

  @push('scripts')
  <script>
    {!! session('success') !!}

  </script>

  @endpush
@endif
