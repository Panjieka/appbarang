@extends('master')

@section('content')
<!-- <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title ">Tambah Data Penjualan</h4>
            </div>
            <div class="card-body">
                <div class="form">
                    <form action="/penjualan" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <div class="form-group">
                            <label for="id_items">Nama Barang</label>
                            <select name="id_items" id="id_items" class="form-control select2" style="color: grey">
                                <option>- - Pilih Barang - -</option>
                                @foreach(\App\Item::all() as $item)
                                <option value="{{$item->id}}" @if($item->id_items == $item->id) selected @endif>{{$item->nama}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="jml_terjual">Jumlah Barang :</label>
                            <input type="text" name="jml_terjual" id="jml_terjual" class="form-control" value="{{$order->jml_terjual}}">
                        </div>
                        
                        <div class="form-group">
                            <label for="tanggal">Tanggal</label>
                            <input type="text" name="tanggal" id="tanggal" class="form-control" value="{{$order->tanggal}}">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        @endsection
        
          </div>
        </div>
      </div>
    </div>
  </div> -->
