@extends('master')
@section('title')
	Order
@endsection
@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
    <div class="card-header card-header-primary">
        <h4 class="card-title">Tabel Penjualan</h4>
        <a href="{{ url('penjualan/create') }}" class="btn btn-sm btn-success shadow-sm"><i class="fa fa-plus"></i> Tambah</a>
    </div>
  </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-hover" style="overflow: auto;">
                <thead>
                    <tr>
                        <td>No</td>
                        <td>Nama Barang</td>
                        <td>Jumlah Barang</td>
                        <td>Tanggal Penjualan</td>
                        <td>Aksi</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($orders as $order)
                    <tr>
                        <td>{{ !empty($i) ? ++$i : $i = 1 }}</td>
                        <td>{{$order->item->nama}}</td>
                        <td>{{$order->jml_terjual}}</td>
                        <td>{{$order->tanggal}}</td>
                        <td>
                        	<form action="/penjualan/{{$order->id}}" method="post">
                             
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editData{{$order->id}}">
                              <i class='fa fa-edit px-1'></i>
                            </button>
                            @csrf
                            @method('delete')
                            <button type="submit" class="btn btn-danger"><i class="fa fa-trash px-1" data-id="{{$order->id}}"></i></button>
                        </form>
                        </td>
                    </tr>
                    <!--modal edit-->

                  <div class="modal fade" id="editData{{$order->id}}" tabindex="-1" aria-labelledby="editDataLabel{{$order->id}}" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="editDataLabel{{$order->id}}" style="color: black">Edit Data Penjualan</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <form action="/penjualan/{{$order->id}}" method="post">
                          @csrf
                          @method('put')
                          <div class="form-group">
                            <label for="id_items" style="color: black">Nama Barang</label>
                            <select name="id_items" id="id_items" class="form-control select2" style="color: black">
                                <option>- - Pilih Barang - -</option>
                                @foreach(\App\Item::all() as $item)
                                <option value="{{$item->id}}" @if($order->id_items == $item->id) selected @endif>{{$item->nama}}</option>
                                @endforeach
                            </select>
                          </div>
                          <div class="form-group">
                            <label for="jml_terjual" style="color: black">Jumlah Barang :</label>
                            <input type="text" style="color: black" name="jml_terjual" id="jml_terjual" class="form-control" value="{{$order->jml_terjual}}">
                          </div>
                        
                          <div class="form-group">
                            <label for="tanggal" style="color: black">Tanggal</label>
                            <input type="text" style="color: black" name="tanggal" id="tanggal" class="form-control" value="{{$order->tanggal}}" readonly>
                          </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-primary">Simpan</button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                    @endforeach
                </tbody>
              </table>
              </div>
            </div>
          </div>
        </div>
    </div>
</div>
@endsection


