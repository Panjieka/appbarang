@extends('master')

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title ">Data Barang</h4>
            <a href="{{ url('data-barang/create') }}" class="btn btn-sm btn-success shadow-sm"><i class="fa fa-plus"></i> Tambah</a>
          </div>

          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                      <td>No</td>
                      <td>Nama</td>
                      <td>Jenis Barang</td>
                      <td>Stok</td>
                      <td>Aksi</td>
                  </tr>
              </thead>
              <tbody>
                  @foreach($items as $item)
                  <tr>
                      <td>{{ !empty($i) ? ++$i : $i = 1 }}</td>
                      <td>{{$item->nama}}</td>
                        <td>
                          {{$item->category->nama}}
                        </td>
                      <td>{{$item->stok}}</td>
                      <td>
                             
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editData{{$item->id}}">
                          <i class='fa fa-edit px-1'></i>
                        </button>
                              @csrf
                              @method('delete')
                              <button type="submit" class="btn btn-danger"><i class="fa fa-trash px-1" data-id="{{$item->id}}"></i></button>
                      </td>
                  </tr>

                  <!--modal edit-->

                  <div class="modal fade" id="editData{{$item->id}}" tabindex="-1" aria-labelledby="editDataLabel{{$item->id}}" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="editDataLabel{{$item->id}}" style="color: black">Edit Data Barang</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <form action="/data-barang/{{$item->id}}" method="post">
                          @csrf
                          @method('put')
                            <div class="form-group">
                            <label for="nama_barang" style="color: black">Nama Barang :</label>
                            <input type="text" style="color: black" name="nama" id="nama" class="form-control" value="{{$item->nama}}">
                        </div>
                        <div class="form-group">
                            <label for="id_category" style="color: black">Jenis Barang</label>
                            <select name="id_category" id="id_category" class="form-control select2" style="color: black">
                                <option>- - Pilih Jenis Barang - -</option>
                                @foreach(\App\Category::all() as $category)
                                <option value="{{$category->id}}" @if($item->id_category == $category->id) selected @endif>{{$category->nama}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="stok" style="color: black">Stok Barang</label>
                            <input type="number" style="color: black" name="stok" id="stok" class="form-control" value="{{$item->stok}}">
                        </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </form>
                      </div>
                    </div>
                  </div>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection