@extends('master')

@section('content')
<!-- <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title ">Tabel Barang</h4>
            </div>
            <div class="card-body">
                <div class="form">
                    <form action="/data-barang" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('put')
                
                        <div class="form-group">
                            <label for="nama_barang">Nama Barang :</label>
                            <input type="text" name="nama" id="nama" class="form-control" value="{{$item->nama}}">
                        </div>
                        <div class="form-group">
                            <label for="id_category">Jenis Barang</label>
                            <select name="id_category" id="id_category" class="form-control select2" style="color: grey">
                                <option>- - Pilih Jenis Barang - -</option>
                                @foreach(\App\Category::all() as $category)
                                <option value="{{$category->id}}" @if($item->id_category == $category->id) selected @endif>{{$category->nama}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="stok">Stok Barang</label>
                            <input type="number" name="stok" id="stok" class="form-control" value="{{$item->stok}}">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        @endsection
        
          </div>
        </div>
      </div>
    </div>
  </div> -->
