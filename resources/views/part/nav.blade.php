<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top " id="navigation-example">
    <div class="container-fluid">
      <div class="navbar-wrapper">
        <a class="navbar-brand" href="javascript:void(0)">Dashboard</a>
      </div>
      <div class="collapse navbar-collapse justify-content-end">
        <form action="/cari" method="GET" class="navbar-form">
          <div class="input-group no-border">
            <input type="text" name="cari" value="{{old('cari')}}" class="form-control" placeholder="Search...">
            <button type="submit" class="btn btn-default btn-round btn-just-icon">
              <i class="material-icons">search</i>
              <div class="ripple-container"></div>
            </button>
          </div>
        </form>
      </div>
    </div>
  </nav>