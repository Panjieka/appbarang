<div class="sidebar" data-background-color="black" data-image="{{asset('/material-dashboard-dark-edition-v2.1.0/assets/img/sidebar-2.jpg')}}">

<div class="logo"><a class="simple-text logo-normal">
    Panji Eka Prasetyo
  </a></div>
<div class="sidebar-wrapper">
  <ul class="nav">
    <li class="nav-item">
      <a class="nav-link" href="./">
        <i class="material-icons">library_books</i>
        <p>Tabel Barang</p>
      </a>
    </li>
    <li class="nav-item ">
        <a class="nav-link" href="./data-barang">
          <i class="material-icons">content_paste</i>
          <p>Data Barang</p>
        </a>
      </li>
    <li class="nav-item ">
      <a class="nav-link" href="./jenis-barang">
        <i class="material-icons">content_paste</i>
        <p>Data Jenis Barang</p>
      </a>
    </li>
    <li class="nav-item ">
      <a class="nav-link" href="./penjualan">
        <i class="material-icons">shop</i>
        <p>Penjualan</p>
      </a>
    </li>
  </ul>
</div>
</div>
<div class="main-panel">


  <script src="{{asset('/material-dashboard-dark-edition-v2.1.0/assets/js/core/jquery.min.js')}}"></script>
  <script src="{{asset('/material-dashboard-dark-edition-v2.1.0/assets/js/core/popper.min.js')}}"></script>
  <script src="{{asset('/material-dashboard-dark-edition-v2.1.0/assets/js/core/bootstrap-material-design.min.js')}}"></script>
  <script src="https://unpkg.com/default-passive-events"></script>
  <script src="{{asset('/material-dashboard-dark-edition-v2.1.0/assets/js/plugins/perfect-scrollbar.jquery.min.js')}}"></script>