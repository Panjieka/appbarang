@extends('master')
@section('title')
	Order
@endsection
@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
    <div class="card-header card-header-primary">
        <h4 class="card-title">Tabel Barang</h4>
    </div>
  </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-hover" style="overflow: auto;">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>NAMA BARANG</th>
                        <th>STOK</th>
                        <th>JUMLAH TERJUAL</th>
                        <th>TANGGAL TRANSAKSI</th>
                        <th>JENIS BARANG</th>
                    </tr>
                </thead>
                <tbody>
                  @foreach ($data as $item)
                    <tr>
                        <td>{{ !empty($i) ? ++$i : $i = 1 }}</td>
                        <td>{{$item->nama}}</td>
                        <td>{{$item->stok}}</td>
                        <td>{{$item->jml_terjual}}</td>
                        <td>{{$item->tanggal}}</td>
                        <td>{{$item->id_category}}</td>
                    </tr>
                    @endforeach
                </tbody>
              </table>
              </div>
            </div>
          </div>
        </div>
    </div>
</div>
@endsection
