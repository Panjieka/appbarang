<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\IndexController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', 'IndexController@index');*/
Route::get('/', '\App\Http\Controllers\IndexController@index');
Route::get('/cari','IndexController@cari');
Route::resource('/data-barang', 'ItemController');

Route::resource('/jenis-barang', 'CategoryController');

Route::resource('/penjualan', 'OrderController');

