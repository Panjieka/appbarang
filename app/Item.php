<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = 'items';
    protected $fillable = ['nama','id_category','stok'];

    //public function category(){
    //	return $this->belongsTo(Category::class,'id');
    //}
    public function category(){
    	return $this->belongsTo('App\Category','id_category');
    }
    public function order(){
    	return $this->hasOne('App\Order');
    }

}
