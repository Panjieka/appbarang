<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';
    protected $fillable = ['id_items','jml_terjual','tanggal'];
    public function item(){
    	return $this->belongsTo('App\Item','id_items');
    }

}
