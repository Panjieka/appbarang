<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class IndexController extends Controller
{
    public function index(){
    
        //mengambil data darri database menggunakan db::table() dan disimpan ke dalam $data
        //menggunakan ->join() untuk menggabungkan tabel lainnya
        //diakhir get() untuk mengambil data array

        $data = DB::table('items')
                ->join('orders', 'orders.id_items', '=', 'items.id')
                //->join('items','items.id_category', '=','categories.nama')
                ->get();

        
        return view('Index')->with('data', $data);
    }

    public function cari(Request $request)
	{
		// menangkap data pencarian
		$cari = $request->cari;
 

		$data = DB::table('items','orders')
		->where('nama','like',"%".$cari."%")
		->paginate();
 
		return view('Index',['data' => $data]);
 
	}
}
