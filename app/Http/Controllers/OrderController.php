<?php

namespace App\Http\Controllers;

use App\Order;
use App\Item;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::with('item')->get();
        
        return view('modul.penjualan.indexPenjualan', compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $items = Item::all();
        return view('modul.penjualan.createPenjualan',compact('items'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules=[
            'id_items' => 'required',
            'jml_terjual' => 'required',
            'tanggal' => 'required',  

        ];
        $status = Order::create([
            'id_items' => $request->id_items,
            'jml_terjual' => $request->jml_terjual,
            'tanggal' => $request->tanggal,
        ]);
        //-- logic untuk mengurangi stok --//
        $items = Item::findOrFail($request->id_items);
        $items->stok -= $request->jml_terjual;
        $items->save();

        if($status) return redirect('/penjualan')->with('success',"Swal.fire(
      'Success!',
      'Data Berhasil Disimpan',
      'success'
    )");
        else return redirect('/penjualan')->with('error','Data gagal Disimpan!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = Order::find($id);
        return view('modul.penjualan.editPenjualan', compact('order'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules=[
            'id_items' => 'required',
            'jml_terjual' => 'required',
            'tanggal' => 'required',  

        ];
        $this->validate($request, $rules);
        $order = Order::find($id);
        $status = $order -> update([
            'id_items' => $request->id_items,
            'jml_terjual' => $request->jml_terjual,
            'tanggal' => $request->tanggal,
        ]);


        if($status) return redirect('/penjualan')->with('success',"Swal.fire(
      'Success!',
      'Data Berhasil Disimpan',
      'success'
    )");
        else return redirect('/penjualan')->with('error','Data gagal Disimpan!!');
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
    Order::where('id', $id)->delete();
       return redirect('/penjualan')->with('success',"Swal.fire(
      'Success!',
      'Data Berhasil Dihapus',
      'success'
    )");
    }
}
