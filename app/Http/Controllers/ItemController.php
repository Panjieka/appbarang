<?php

namespace App\Http\Controllers;

use App\Item;
use Illuminate\Http\Request;
Use App\Category;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Item::with('category')->get();
        
        return view('modul.databarang.indexBarang', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('modul.databarang.createBarang',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules=[
            'nama' => 'required',
            'id_category' => 'required',
            'stok' => 'required|numeric',  

        ];
        $status = Item::create([
            'nama' => $request->nama,
            'id_category' => $request->id_category,
            'stok' => $request->stok,
        ]);

        if($status) return redirect('/data-barang')->with('success',"Swal.fire(
      'Success!',
      'Data Berhasil Disimpan',
      'success'
    )");
        else return redirect('/data-barang')->with('error','Data gagal Disimpan!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show($item)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Item::find($id);
        return view('modul.databarang.editBarang', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
        $rules=[
            'nama' => 'required',
            'id_category' => 'required',
            'stok' => 'required|numeric',

        ];
        $this->validate($request, $rules);
        $item = Item::find($id);
            $status = $item ->update([
                'nama' => $request->nama,
                'id_category' => $request->id_category,
                'stok' => $request->stok,
            ]);

            
        if($status) return redirect('/data-barang')->with('success',"Swal.fire(
            'Success!',
            'Data Berhasil Disimpan',
            'success'
          )");
              else return redirect('/data-barang')->with('error','Data gagal Disimpan!!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Item::where('id', $id)->delete();
       return redirect('/data-barang')->with('success',"Swal.fire(
      'Success!',
      'Data Berhasil Dihapus',
      'success'
    )");
    }
}
